/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cVyraz.cpp
 * trida cVyraz, generuje kod pro vyraz
 */

#include <iostream>
#include "cVyraz.h"
#include "cTerm.h"

using namespace std;

cVyraz::cVyraz  ( cTerm term )
{
	cTerm    *t = new cTerm ( term );
	this->empty = false;
	this->term  = t;
}

cVyraz::cVyraz  ( )
{
	this->empty = true;
}

cVyraz::cVyraz ( const cVyraz& v )
{
	empty = v.empty;
	term  = v.term;
}

cVyraz::~cVyraz ( )
{ /* nothing here */ }

void cVyraz::eval ( )
{
	if ( empty ) return;
	(*this->term).eval();
}

string cVyraz::getTyp ( )
{
	if ( empty ) return "int"; //FIXME tohle snad nikdy nemuze nastat
	string typ = (*this->term).getTyp();	
	return typ;
}

/* EOF */
