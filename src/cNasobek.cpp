/* Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cNasobek.cpp
 * trida cNasobek, generuje kod pro vypocet nasobeni
 */

#include <iostream>
#include "cNasobek.h"
#include "cFaktor.h"

using namespace std;

cNasobek::cNasobek ( cFaktor levy, cFaktor pravy )
{
	cFaktor *l  = new cFaktor ( levy  );
	cFaktor *p  = new cFaktor ( pravy );
	this->levy  = l;
	this->pravy = p;
}

cNasobek::cNasobek ( const cNasobek& n )
{
	levy  = n.levy;
	pravy = n.pravy;
}

cNasobek::~cNasobek()
{ /* nothing here */ }

void cNasobek::eval ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	bool x = ( ltyp != rtyp ); // jeden z nich je urcite float
	
	(*this->pravy).eval();
	if ( x && rtyp == "int" ) cout << "i2d" << endl;
	(*this->levy).eval();
	if ( x && ltyp == "int" ) cout << "i2d" << endl;

	if ( x || ltyp == "float" ) cout << "dmul" << endl;
	else cout << "imul" << endl;
}

string cNasobek::getTyp ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	if ( ltyp == "float" || rtyp == "float" )
		return "float";
	else
		return "int";
}

/* EOF */

