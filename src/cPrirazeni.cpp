/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPrirazeni.cpp
 * trida cPrirazeni, generuje kod pro ulozeni vyrazu do promenne
 */

#include <iostream>
#include <stdlib.h>
#include "cPrirazeni.h"
#include "cIdentifikator.h"

using namespace std;

cPrirazeni::cPrirazeni ( cIdentifikator ident, cVyraz vyraz )
{
	cIdentifikator *i = new cIdentifikator ( ident );
	cVyraz         *v = new cVyraz         ( vyraz );
	this->empty       = false;
	this->ident       = i;
	this->vyraz       = v;
}

cPrirazeni::cPrirazeni ( )
{
	this->empty = true;
}

cPrirazeni::cPrirazeni ( const cPrirazeni& p )
{
	empty = p.empty;
	ident = p.ident;
	vyraz = p.vyraz;
}

cPrirazeni::~cPrirazeni ( )
{ /* nothing here */ }

void cPrirazeni::eval ( )
{
	if ( empty ) return;
	cout << "; Prirazeni" << endl;
	string identname = (*this->ident).getName  ( );
	string typvyraz  = (*this->vyraz).getTyp   ( );
	int    identid   = cTS::instance()->getID  ( identname );
	string typident  = cTS::instance()->getTyp ( identname );

	// Vypsani vyrazu
	(*this->vyraz).eval ( );

	// Ulozeni do promenne
	if ( typvyraz != typident )
	{
		if ( typvyraz == "int" ) // typident je float
		{
			cout << "i2d" << endl;
			cout << "dstore " << identid;
			cout << " ;" << identname << endl;
		}
		else
		{
			// promenna je int a vyraz float - chyba
			cerr << "Pokud o prirazeni float do promenne typu int!" << endl;
			exit(1); // FIXME -- kam to napsat? na stderr???
		}
	}
	else
	{
		if ( typident == "int"   ) cout << "i";
		if ( typident == "float" ) cout << "d";
		cout << "store " << identid;
		cout << " ;" << identname << endl;
	}
}

/* EOF */
