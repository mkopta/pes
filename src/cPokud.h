/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPokud.h
 * hlavickovy soubor pro tridu cPokud
 */

#ifndef __cPokud_h_2469818209798749809238972
#define __cPokud_h_2469818209798749809238972

class cBlok;

#include "cUzel.h"
#include "cPodminka.h"
#include "cBlok.h"

class cPokud : public cUzel
{
	private:
		cPodminka *podminka;
		cBlok     *telo;
		cBlok     *jinak;

	public:
		cPokud    ( cPodminka podminka, cBlok telo, cBlok jinak );
		cPokud    ( const cPokud& p );
		virtual ~cPokud   ( );
		void eval ( );
};

#endif /* __cPokud_h_2469818209798749809238972 */

/* EOF */
