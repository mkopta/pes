/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cBlok.h
 * hlavickovy soubor pro tridu cBlok
 */

#ifndef __cBlok_h_90098993519440926511
#define __cBlok_h_90098993519440926511

class cPrikaz;

#include "cUzel.h"
#include "cPrikaz.h"

class cBlok : public cUzel
{
	private:
		cPrikaz  *prikaz;
		cBlok    *next;
		bool      empty;

	public:
		cBlok     ( cPrikaz prikaz, cBlok next );
		cBlok     ( const cBlok& b );
		cBlok     ( );
		virtual ~cBlok ( );
		void eval ( );
};

#endif /* __cBlok_h_90098993519440926511 */

/* EOF */
