/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPrikaz.h
 * hlavickovy soubor pro tridu cPrikaz
 */

#ifndef __cPrikaz_h_887624443451334232111780
#define __cPrikaz_h_887624443451334232111780

class cPokud;

#include "cUzel.h"
#include "cPokud.h"
#include "cDokud.h"
#include "cVraceni.h"
#include "cPrirazeni.h"

class cDokud;

class cPrikaz : public cUzel
{
	private:
		cPokud     *pokud;
		cDokud     *dokud;
		cVraceni   *vraceni;
		cPrirazeni *prirazeni;
		bool        empty;

	public:
		cPrikaz   ( cPokud     prikaz );
		cPrikaz   ( cDokud     prikaz );
		cPrikaz   ( cVraceni   prikaz );
		cPrikaz   ( cPrirazeni prikaz );
		cPrikaz   ( const cPrikaz& p  );
		cPrikaz   ( );
		virtual ~cPrikaz  ( );
		void eval ( );
};

#endif /* __cPrikaz_h_887624443451334232111780 */

/* EOF */
