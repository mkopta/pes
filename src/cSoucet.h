/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cSoucet.h
 * hlavickovy soubor pro tridu cSoucet
 */

#ifndef __cSoucet_h_979879997623442323437218
#define __cSoucet_h_979879997623442323437218

class cTerm;

#include "cUzel.h"
#include "cTerm.h"

class cSoucet : public cUzel
{
	private:
		cTerm   *levy;
		cTerm   *pravy;
	public:
		         cSoucet ( cTerm levy, cTerm pravy );
		         cSoucet ( const cSoucet& s );
		virtual ~cSoucet ( );
		void     eval    ( );
		string   getTyp  ( );
};

#endif /* __cSoucet_h_979879997623442323437218 */

/* EOF */
