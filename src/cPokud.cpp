/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPokud.cpp
 * trida cPokud, generuje kod pro vetveni
 */

#include <iostream>
#include <sstream>
#include "cPokud.h"
#include "cPodminka.h"
#include "cTS.h"

using namespace std;

// int to string conversion trick
template <class T>
inline std::string to_string (const T& t)
{
	std::stringstream ss;
	ss << t;
	return ss.str();
}

cPokud::cPokud ( cPodminka podminka, cBlok telo, cBlok jinak )
{
	cPodminka   *p = new cPodminka ( podminka );
	cBlok       *t = new cBlok     ( telo     );
	cBlok       *j = new cBlok     ( jinak    );

	this->podminka = p;
	this->telo     = t;
	this->jinak    = j;
}

cPokud::cPokud ( const cPokud& p )
{
	podminka = p.podminka;
	telo     = p.telo;
	jinak    = p.jinak;
}
		
cPokud::~cPokud()
{ /* nothing here */ }

void cPokud::eval ()
{
	int labnum = cTS::instance()->getNextLab();
	cout << "; Vetveni" << endl;
	cout << "vetveni_" << labnum << ": " << endl;
	(*this->podminka).setLabel("vetveni_stred_"+to_string(labnum));
	(*this->podminka).eval();
	(*this->telo).eval();
	cout << "vetveni_stred_" << labnum << ": " << endl;
	(*this->jinak).eval();
	cout << "vetveni_konec_" << labnum << ": " << endl;
}

/* EOF */
