/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Nov 2008
 * synan.h
 * knihovna pro syntakticky analyzator jazyka pes
 */

#ifndef __synan_h__6437864780918731267838__
#define __synan_h__6437864780918731267838__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "cProgram.h"

extern cProgram parse(char *filename);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __synan_h__6437864780918731267838__ */

/* EOF */
