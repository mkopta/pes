/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cDokud.h
 * hlavickovy soubor pro tridu cDokud
 */

#ifndef __cDokud_h_8392789468933892389
#define __cDokud_h_8392789468933892389

class cBlok;

#include "cUzel.h"
#include "cPodminka.h"
#include "cBlok.h"

class cDokud : public cUzel
{
	private:
		cPodminka *podminka;
		cBlok     *blok;
		bool       empty;

	public:
		cDokud    ( cPodminka podminka, cBlok blok );
		cDokud    ( const cDokud& d );
		cDokud    ( );
		virtual ~cDokud   ( );
		void eval ( );
};

#endif /* __cDokud_h_8392789468933892389 */

/* EOF */
