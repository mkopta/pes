/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Nov 2008
 * pesc.c
 * Kompilator jazyka pes
 */

#include <iostream>
#include <map>
#include <list>
#include "synan.h"
#include "cProgram.h"
#include "cTS.h"

using namespace std;

cTS* cTS::tsinstance = 0;

int main ( int argc, char *argv[] )
{
	if ( argc != 2 )
	{
		cout << "Usage: " <<  argv[0] << " <source.pes>" << endl;
		return (1);
	}

	cProgram ast = parse ( argv[1] );

	ast.eval();

	return (0);
}

/* EOF */
