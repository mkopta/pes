/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec, 2008
 * cTS.cpp
 * Tabulka symbolu (promenych, typy a id, labely)

____________________________ ________________________
|Tabulka A                 | | Tabulka B            |
+-----------------+--------+ +---------+------------+
|string identname | int id | | int id  | string typ |
+-----------------+--------+ +---------+------------+
| pi              |    0 <-----> 0     | float      |
| size            |    1   | |   1     | int        |
| delta           |    2   | |   2     | float      |
| ...             |    .   | |   .     | ...        |
| ...             |    .   | |   .     | ...        |
+-----------------+--------+ +---------+------------+
 ( std::map<string, int> )   ( std::map<int, string> )
 */

#include <iostream>
#include <map>
#include <list>
#include <utility> // make_pair
#include "cTS.h"

using namespace std;

cTS::cTS ( )
{
	this->lastID  = -1;
	this->lastLAB = -1;
	A.clear ( );
	B.clear ( );
}

int cTS::getNextLab ( )
{
	return ++lastLAB;
}

bool cTS::jeDeklarovan ( string identname )
{
	map <string, int>::iterator iter;
	iter = A.find ( identname );
	return ( iter != A.end() ); // true if is declared
}

void cTS::add ( string identname, string typ )
{
	int id = ++lastID;
	if ( typ == "float" ) lastID++; // float potrebuje dva registry
	A.insert ( make_pair ( identname , id ) ) ;
	B.insert ( make_pair ( id , typ       ) ) ;
}

int cTS::getID ( string identname )
{
	map <string, int>::iterator iter;
	iter = A.find ( identname );
	if ( iter != A.end() )
	{
		return iter->second;
	}
	else
	{
		return -1;
	}
}

string cTS::getTyp ( string identname )
{
	map <string, int>::iterator iterA;
	map <int, string>::iterator iterB;
	iterA = A.find ( identname );
	if ( iterA != A.end() )
	{
		int id = iterA->second;
		iterB = B.find ( id );
		if ( iterB != B.end() )
		{
			return iterB->second;
		}
		else
		{
			// FIXME -- MORE FATAL ERROR
			return "";
		}
	}
	else
	{
		// FIXME -- FATAL ERROR
		return "";
	}
}

string cTS::getIdentname ( int ID )
{
	map <string, int>::iterator iterA;

	// Pruchod tabulkou A
	for( map<string, int>::const_iterator it = A.begin(); it != A.end(); ++it )
	{
		if ( it->second == ID )
		{
			return it->first;
		}
	}
	return ""; // FIXME, ERROR
}

int cTS::getPocetProm ( )
{
	return this->lastID;
}

list<int> cTS::getIDpromennych ( )
{
	map  <string, int>::iterator iterA;
	list <int> IDs;
	// Pruchod tabulkou A
	for( map<string, int>::const_iterator it = A.begin(); it != A.end(); ++it )
	{
		IDs.push_back( it->second );
	}
	return IDs;
}

/* EOF */
