/* Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPrikaz.cpp
 * trida cPrikaz, generuje kod prikazu
 */

#include <iostream>
#include "cPrikaz.h"
#include "cPokud.h"
#include "cDokud.h"
#include "cVraceni.h"
#include "cPrirazeni.h"

using namespace std;

cPrikaz::cPrikaz ( cPokud     prikaz )
{
	cPokud *p = new cPokud ( prikaz );
	this->empty     = false;
	this->pokud     = p;
	this->dokud     = 0;
	this->prirazeni = 0;
	this->vraceni   = 0;
}

cPrikaz::cPrikaz ( cDokud     prikaz )
{
	cDokud *p = new cDokud ( prikaz );
	this->empty     = false;
	this->pokud     = 0;
	this->dokud     = p;
	this->prirazeni = 0;
	this->vraceni   = 0;
}

cPrikaz::cPrikaz ( cPrirazeni prikaz )
{
	cPrirazeni *p = new cPrirazeni ( prikaz );
	this->empty     = false;
	this->pokud     = 0;
	this->dokud     = 0;
	this->prirazeni = p;
	this->vraceni   = 0;
}

cPrikaz::cPrikaz ( cVraceni   prikaz )
{
	cVraceni *p = new cVraceni ( prikaz );
	this->empty     = false;
	this->pokud     = 0;
	this->dokud     = 0;
	this->prirazeni = 0;
	this->vraceni   = p;
}

cPrikaz::cPrikaz     ( const cPrikaz& p )
{
	empty     = p.empty;
	pokud     = p.pokud; 
	dokud     = p.dokud;
	vraceni   = p.vraceni;
	prirazeni = p.prirazeni;
}

cPrikaz::cPrikaz ( )
{
	this->empty = true;
}

cPrikaz::~cPrikaz()
{}

void cPrikaz::eval()
{
	if ( empty ) return;

	if ( this->pokud )
	{
		(*this->pokud).eval();
	}
	else if ( this->dokud )
	{
		(*this->dokud).eval();
	}
	else if ( this->vraceni )
	{
		(*this->vraceni).eval();
	}
	else if ( this->prirazeni )
	{
		(*this->prirazeni).eval();
	}
}

/* EOF */
