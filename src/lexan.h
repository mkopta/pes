/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Nov 2008
 * lexan.h
 * knihovna pro lexikalni analyzator jazyka pes
 */

#ifndef __lexan_h__3742897392862899478923__
#define __lexan_h__3742897392862899478923__

#define LEX_NULL          0    /* EOF & pocatecni stav */
#define LEX_ERROR         1    /* neznamy element, chyba */

#define LEX_ADD           2    /* + addition */
#define LEX_SUB           3    /* - minus */

#define LEX_DIV           4    /* / */
#define LEX_MUL           5    /* * multiplication */

#define LEX_LCB           6    /* { left  curly bracket */
#define LEX_RCB           7    /* } right curly bracket */

#define LEX_LBB           8    /* [ left  box   bracket */
#define LEX_RBB           9    /* ] right box   bracket */

#define LEX_LAB          10    /* < left  angle bracket */
#define LEX_RAB          11    /* > right angle bracket */

#define LEX_EQ           12    /* =  equal */
#define LEX_IEQ          13    /* == is equals */
#define LEX_NEQ          14    /* != not equals */

#define LEX_PROGRAM      15    /* keyword "program" */
#define LEX_PROMENA      16    /* keyword "promena" */
#define LEX_DOKUD        17    /* keyword "dokud" */
#define LEX_POKUD        18    /* keyword "pokud" */
#define LEX_JINAK        19    /* keyword "jinak" */
#define LEX_VRAT         20    /* keyword "vrat" */

#define LEX_IDENT        21    /* identifikator, ne klicove slovo */

#define LEX_INT          22    /* number */
#define LEX_FLOAT        23    /* number dot number */
#define LEX_INT_KW       24    /* promena int $x */
#define LEX_FLOAT_KW     25    /* promena float $x */


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */ 

extern int     lex_int;              /* for LEX_INT */
extern double  lex_float;            /* for LEX_FLOAT */
extern char    lex_ident[2048];      /* for LEX_IDENT */
extern char    lex_prgname[2048];    /* for LEX_PROGRAM */
extern int     linenum;
extern char    unknown_char;

int  getNextToken();
int  lexanInit(char *filename);
void lexanDone();

#ifdef __cplusplus
}
#endif /* __cplusplus */ 


#endif /* __lexan_h__3742897392862899478923__ */

/* EOF */
