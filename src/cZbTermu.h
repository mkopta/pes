/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cZbTermu.h
 * hlavickovy soubor pro tridu cZbTermu
 */

#ifndef __cZbTermu_h_12347484214598649
#define __cZbTermu_h_12347484214598649

#include "cUzel.h"
#include "cFaktor.h"

class cZbTermu : public cUzel
{
	private:
		bool     empty;
		cFaktor *faktor;

	public:
		         cZbTermu ( cFaktor faktor );
		         cZbTermu ( );
		         cZbTermu ( const cZbTermu& z );
		virtual ~cZbTermu ( );
		void     eval     ( );
		string   getTyp   ( );
};

#endif /* __cZbTermu_h_12347484214598649 */

/* EOF */
