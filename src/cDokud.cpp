/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cDokud.cpp
 * trida cDokud, generuje kod pro cyklus
 */

#include <iostream>
#include <sstream>
#include "cDokud.h"
#include "cPodminka.h"
#include "cBlok.h"
#include "cTS.h"

using namespace std;

// int to string conversion trick
template <class T>
inline std::string to_string (const T& t)
{
	std::stringstream ss;
	ss << t;
	return ss.str();
}

cDokud::cDokud ( cPodminka podminka, cBlok blok )
{
	cPodminka   *p = new cPodminka ( podminka );
	cBlok       *b = new cBlok     ( blok     );
	this->empty    = false;
	this->podminka = p;
	this->blok     = b;
}

cDokud::cDokud ( const cDokud& d )
{
	empty    = d.empty;
	podminka = d.podminka;
	blok     = d.blok; 
}

cDokud::cDokud ( )
{
	this->empty    = true;
	this->podminka = 0;
	this->blok     = 0;
}

cDokud::~cDokud ( )
{ /* nothing here */ }

void cDokud::eval ( )
{
	if ( empty ) return;
	int labnum = cTS::instance()->getNextLab();
	cout << "; Cyklus" << endl;
	cout << "cyklus_" << labnum << ": " << endl;
	(*this->podminka).setLabel("cyklus_konec_"+to_string(labnum));
	(*this->podminka).eval();
	(*this->blok).eval();
	cout << "goto cyklus_" << labnum << endl;
	cout << "cyklus_konec_" << labnum << ": " << endl;
}

/* EOF */
