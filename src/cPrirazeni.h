/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPrirazeni.h
 * hlavickovy soubor pro tridu cPrirazeni
 */

#ifndef __cPrirazeni_h_553762679080981278979
#define __cPrirazeni_h_553762679080981278979

#include "cUzel.h"
#include "cIdentifikator.h"
#include "cVyraz.h"

class cPrirazeni : public cUzel
{
	private:
		cIdentifikator *ident;
		cVyraz         *vyraz;
		bool            empty;

	public:
		cPrirazeni     ( cIdentifikator ident, cVyraz vyraz );
		cPrirazeni     ( const cPrirazeni& p );
		cPrirazeni     ( );
		virtual ~cPrirazeni    ( );
		void eval      ( );
};

#endif /* __cPrirazeni_h_553762679080981278979 */ 

/* EOF */
