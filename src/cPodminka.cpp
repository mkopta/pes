/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPodminka.cpp
 * trida cPodminka, generuje kod pro podminku
 *  (ktera se vyskytuje ve vetveni nebo v cyklu)
 * 
 * INTEGER
 *   if_icmple label ---> ( x > y )
 *   if_icmpge label ---> ( x < y )
 *   if_icmpne label ---> ( x == y )
 *   if_icmpeq label ---> ( x != y )
 * DOUBLE
 *   dcmpg
 *   ifne label ; ( x == y )
 *   ifeq label ; ( x != y )
 *   ifle label ; ( x > y )
 *   ifge label ; ( x < y )
 */

#include <iostream>
#include "cPodminka.h"
#include "cFaktor.h"

using namespace std;

cPodminka::cPodminka ( cFaktor levy, std::string relace, cFaktor pravy )
{
	cFaktor  *l  = new cFaktor ( levy  );
	cFaktor  *p  = new cFaktor ( pravy );
	this->levy   = l;
	this->pravy  = p;
	this->relace = relace;
	this->label  = "";
}

cPodminka::cPodminka ( const cPodminka& p )
{
	levy   = p.levy;
	pravy  = p.pravy;
	relace = p.relace;
	label  = p.label;
}

cPodminka::~cPodminka ( )
{ /* nothing here */ }

void cPodminka::eval ( )
{
	string typlevy  = (*this->levy).getTyp();
	string typpravy = (*this->pravy).getTyp();
	if ( typlevy != typpravy )
	{ // POROVNANI FLOATU + pretypovani
		(*this->levy).eval();
		if ( typlevy == "int" ) cout << "i2d" << endl;
		(*this->pravy).eval();
		if ( typpravy == "int" ) cout << "i2d" << endl;
		// ted jsou oba float
		cout << "dcmpg" << endl;
		cout << "if";
		if ( this->relace == "==" ) cout << "ne ";
		if ( this->relace == "!=" ) cout << "eq ";
		if ( this->relace == "<"  ) cout << "ge ";
		if ( this->relace == ">"  ) cout << "le ";
		cout << this->label << endl; // kam skocit
		return;
	}
	else
	{
		(*this->levy).eval();
		(*this->pravy).eval();
		if ( typlevy == "int" ) // typlevy je taky int (predchozi podminka)
		{ // POROVNANI INTEGERU
			cout << "if_icmp";
			if ( this->relace == "==" ) cout << "ne ";
			if ( this->relace == "!=" ) cout << "eq ";
			if ( this->relace == "<"  ) cout << "ge ";
			if ( this->relace == ">"  ) cout << "le ";
			cout << this->label << endl; // kam skocit
			return;
		}
		else // typy jsou float
		{ // POROVNANI FLOATU
			cout << "dcmpg" << endl;
			cout << "if";
			if ( this->relace == "==" ) cout << "ne ";
			if ( this->relace == "!=" ) cout << "eq ";
			if ( this->relace == "<"  ) cout << "ge ";
			if ( this->relace == ">"  ) cout << "le ";
			cout << this->label << endl; // kam skocit
			return;
		}
	}
}

void cPodminka::setLabel ( string givenlabel )
{
	this->label = givenlabel;
}

/* EOF */
