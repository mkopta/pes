/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cProgram.cpp
 * trida cProgram, generuje kod programu
 */

#include <iostream>
#include <list>
#include "cProgram.h"
#include "cBlok.h"
#include "cTS.h"

using namespace std;

cProgram::cProgram  ( std::string prgname, cBlok blok )
{
	cBlok        *b = new cBlok ( blok );
	this->emptyblok = false;
	this->prgname   = prgname;
	this->blok      = b;
}

cProgram::cProgram  ( )
{
	this->emptyblok = true;
}

cProgram::cProgram ( const cProgram& p )
{
	emptyblok = p.emptyblok;
	prgname   = p.prgname;
	blok      = p.blok;
}

cProgram::~cProgram ( )
{ /* nothing here */ }

void cProgram::eval ( )
{
	int pp = cTS::instance()->getPocetProm();
	pp++; // ugly hack :-)
	if ( pp == 0 )
	{
		pp = 2; // misto pro vstupni promenne
	}
	cout << "; PROGRAM: " << prgname << endl;
	cout << ".class public " << prgname << endl;
	cout << ".super java/lang/Object" << endl;
	cout << endl;
	cout << ".method public static main([Ljava/lang/String;)V" << endl;
	cout << ".limit stack  " << pp*10 << endl;
	cout << ".limit locals " << pp << endl;
	if ( emptyblok ) return;
	cout << "; Inicializace promennych" << endl;
	initPromennych ( );
	cout << "; Prelozene prikazy" << endl;
	(*this->blok).eval();
	cout << "; Konec sveta" << endl;
	cout << "return" << endl; // pro pripad ze programator zapomel
	cout << ".end method" << endl;
}

void cProgram::initPromennych ( )
{
	string fdefault = "-273.15"; // Absolut zero
	string idefault = "-273"; // Absolut zero
	string name;
	string typ;
	list<int> IDs = cTS::instance()->getIDpromennych();
	list<int>::iterator i;

	for ( i = IDs.begin(); i != IDs.end(); ++i )
	{
		name = cTS::instance()->getIdentname ( *i );
		typ  = cTS::instance()->getTyp ( name );
		if ( typ == "float" )
		{
			cout << "ldc2_w ";
			cout << fdefault << endl;
			cout  << "dstore ";
			cout << *i ; // ID promenne
			cout << " ;" << name;
		}
		else // if ( typ == "int" )
		{
			cout << "ldc    ";
			cout << idefault << endl;
			cout << "istore ";
			cout << *i ; // ID promenne
			cout << " ;" << name;
		}
		cout << endl;
	}
}

/* EOF */
