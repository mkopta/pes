/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cInt.cpp
 * trida cInt, generuje kod pro pouzivani celociselnych konstant
 */

#include <iostream>
#include "cInt.h"

using namespace std;

cInt::cInt ( int hodnota )
{
	this->hodnota = hodnota;
}

cInt::cInt ( const cInt& i )
{
	hodnota = i.hodnota;
}

cInt::~cInt ()
{ /* nothing here */ }

void cInt::eval ()
{
	cout << "ldc " << hodnota << endl;
}

/* EOF */
