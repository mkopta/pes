/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cVraceni.h
 * hlavickovy soubor pro tridu cVraceni
 */

#ifndef __cInt_h_446357889709983907893278978
#define __cInt_h_446357889709983907893278978

#include "cUzel.h"

class cInt : public cUzel
{
	private:
		int hodnota;

	public:
		cInt      ( int hodnota );
		cInt      ( const cInt& i );
		virtual ~cInt     ( );
		void eval ( );
};

#endif /* __cInt_h_446357889709983907893278978 */

/* EOF */
