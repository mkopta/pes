/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cBlok.cpp
 * trida cBlok, generuje kod bloku prikazu
 */

#include <iostream>
#include "cBlok.h"
#include "cPrikaz.h"

using namespace std;

cBlok::cBlok     ( cPrikaz prikaz, cBlok next )
{
	this->empty = false;
	cPrikaz   *p = new cPrikaz ( prikaz );
	cBlok     *b = new cBlok   ( next   );
	this->prikaz = p;
	this->next   = b;
}

cBlok::cBlok     ( const cBlok& b )
{
	empty  = b.empty;
	prikaz = b.prikaz;
	next   = b.next;
}

cBlok::cBlok     ( )
{
	this->empty = true;
}

cBlok::~cBlok    ( )
{ /* nothing here */ }

void cBlok::eval ( )
{
	if ( empty ) return;
	(*this->prikaz).eval();
	(*this->next).eval();
}

/* EOF */
