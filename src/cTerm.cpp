/* Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cTerm.cpp
 * trida cTerm, generuje kod pro Term (cast vyrazu)
 */

#include <iostream>

#include "cTerm.h"
#include "cSoucet.h"
#include "cRozdil.h"

using namespace std;


cTerm::cTerm ( cSoucet soucet )
{
	cSoucet    *s = new cSoucet ( soucet );
	this->empty   = false;
	this->soucet  = s;
	this->rozdil  = 0;
	this->zbtermu = 0;
}

cTerm::cTerm ( cRozdil rozdil )
{
	cRozdil    *r = new cRozdil ( rozdil );
	this->empty   = false;
	this->soucet  = 0;
	this->rozdil  = r;
	this->zbtermu = 0;
}

cTerm::cTerm ( cZbTermu zbtermu )
{
	cZbTermu   *z = new cZbTermu ( zbtermu );
	this->empty   = false;
	this->soucet  = 0;
	this->rozdil  = 0;
	this->zbtermu = z;
}

cTerm::cTerm ( )
{
	this->empty = true;
}

cTerm::cTerm ( const cTerm& t )
{
	empty   = t.empty;
	soucet  = t.soucet;
	rozdil  = t.rozdil;
	zbtermu = t.zbtermu;
}

cTerm::~cTerm ( )
{ /* nothing here */ }

void cTerm::eval ( )
{
	if ( empty ) return ;
	if ( this->soucet ) (*this->soucet).eval();
	else if ( this->rozdil ) (*this->rozdil).eval();
	else if ( this->zbtermu ) (*this->zbtermu).eval();
}

string cTerm::getTyp ( )
{
	if ( empty ) return "int"; // FIXME, nikdy se nestane??
	string typ;
	if ( this->soucet ) typ = (*this->soucet).getTyp();
	else if ( this->rozdil ) typ = (*this->rozdil).getTyp();
	else /* if ( this->zbtermu ) */ typ = (*this->zbtermu).getTyp();
	return typ;
}

/* EOF */
