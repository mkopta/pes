/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Nov 2008
 * lexan.c
 * lexikalni analyzator jazyka pes
 * 
 * otevre soubor (lexanInit), predava jednotlive tokeny (getNextToken)
 * a po skonceni zavre soubor (lexanDone)
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "lexan.h"

static FILE *fp; /* vstupni soubor */
int    lex_int;
double lex_float;
char   lex_ident[2048];
char   lex_prgname[2048];
int    linenum;
char   unknown_char;

int lexanInit(char *filename)
{
	fp = fopen(filename, "rt");
	return ( fp != NULL );
	linenum = 1;
	unknown_char = '\0';
}

void lexanDone()
{
	fclose(fp);
}

int getNextToken()
{
	int znak; /* prave ziskany znak */
	int stav = 0; /* stav ve kterem se nachazime */
	int identpos = 0; /* delka doposud prectene casti identifikatoru */
	int prognamepos = 0; /* delka doposud prectene casti nazvu prog. */
	double numpos = 0.1; /* pro cteni cisel */

	if (!fp){ return LEX_ERROR; } /* problem se vstupnim souborem */

	while(1)
	{
		znak = fgetc(fp); /* nacteni znaku ze vstupu */
		switch (stav)
		{
			case 0:
				/* Preskakujeme whitespace */
				if ( znak == ' ' || znak == '\t' )
				{
					break;
				}
				if ( znak == '\n' || znak == '\r' )
				{
					++linenum;
					break;
				}
				/* program | promena | pokud */
				if ( znak == 'p' ){ stav = 3;  break; }
				/* jinak */
				if ( znak == 'j' ){ stav = 30; break; }
				/* dokud */
				if ( znak == 'd' ){ stav = 15; break; }
				/* vrat */
				if ( znak == 'v' ){ stav = 19; break; }
				/* int */
				if ( znak == 'i' ){ stav = 35; break; }
				/* float */
				if ( znak == 'f' ){ stav = 37; break; }
				/* = | == */
				if ( znak == '=' ){ stav = 1;  break; }
				/* != */
				if ( znak == '!' ){ stav = 2;  break; }
				/* $identificator */
				if ( znak == '$' ){ stav = 22; break; }
				/* number */
				if (isdigit(znak))
				{
					lex_int = znak - '0';
					lex_float = znak - '0';
					stav = 24;
					break;
				}
				/* jednoznakove elementy */
				if ( znak == '[' ){ return LEX_LBB; }
				if ( znak == ']' ){ return LEX_RBB; }
				if ( znak == '{' ){ return LEX_LCB; }
				if ( znak == '}' ){ return LEX_RCB; }
				if ( znak == '<' ){ return LEX_LAB; }
				if ( znak == '>' ){ return LEX_RAB; }
				if ( znak == '+' ){ return LEX_ADD; }
				if ( znak == '-' ){ return LEX_SUB; }
				if ( znak == '*' ){ return LEX_MUL; }
				if ( znak == '/' ){ return LEX_DIV; }
				if ( znak == EOF ){ return LEX_NULL;}
				/* neznamy element */
				{
					unknown_char = znak;
					return LEX_ERROR;
				}

			case 1: /* '='..? */
				if ( znak == '=' ) { return LEX_IEQ; }
				else if ( znak == ' ' || znak == '\t' ) 
				{
					return LEX_EQ;
				}
				else if ( znak == '\r' || znak	== '\n')
				{
					linenum++;
					return LEX_EQ;
				}
				else
				{
					ungetc(znak, fp);
					return LEX_EQ;
				}

			case 2: /* '!'..? */
				if ( znak == '=' ) { return LEX_NEQ; }
				else { return LEX_ERROR; }

			case 3: /* 'p'..? */
				if ( znak == 'r' ){ stav = 4; break; }
				else if ( znak == 'o' ){ stav = 12; break; } /* pokud */
				else { return LEX_ERROR; }

			case 4: /* 'pr'..? */
				if ( znak == 'o' ){ stav = 5; break; }
				else { return LEX_ERROR; }

			case 5: /* 'pro'..? */
				if      ( znak == 'g' ){ stav = 6; break; }
				else if ( znak == 'm' ){ stav = 9; break; }
				else { return LEX_ERROR; }

			case 6: /* 'prog'..? */
				if ( znak == 'r' ){ stav = 7; break; }
				else { return LEX_ERROR; }

			case 7: /* 'progr'..? */
				if ( znak == 'a' ){ stav = 8; break; }
				else { return LEX_ERROR; }

			case 8: /* 'progra'..? */
				if ( znak == 'm' )
				{
					/* matched keyword 'program' */
					/* now at least one whitespace after */
					stav = 27;
					break;
				}
				else { return LEX_ERROR; }

			case 9: /* 'prom'..? */
				if ( znak == 'e' ){ stav = 10; break; }
				else { return LEX_ERROR; }
				
			case 10: /* 'prome'..? */
				if ( znak == 'n' ){ stav = 11; break; }
				else { return LEX_ERROR; }

			case 11: /* 'promen'..? */
				if ( znak == 'n' ) { stav = 34; break; }
				else { return LEX_ERROR; }

			case 12: /* 'po'..? */
				if ( znak == 'k' ){ stav = 13; break; }
				else { return LEX_ERROR; }

			case 13: /* 'pok'..? */
				if ( znak == 'u' ){ stav = 14; break; }
				else { return LEX_ERROR; }

			case 14: /* 'poku'..? */
				if ( znak == 'd' ){ return LEX_POKUD; }
				else { return LEX_ERROR; }

			case 15: /* 'd'..? */
				if ( znak == 'o' ){ stav = 16; break; }
				else { return LEX_ERROR; }

			case 16: /* 'do'..? */
				if ( znak == 'k' ){ stav = 17; break; }
				else { return LEX_ERROR; }

			case 17: /* 'dok'..? */
				if ( znak == 'u' ){ stav = 18; break; }
				else { return LEX_ERROR; }

			case 18: /* 'doku'..? */
				if ( znak == 'd' ){ return LEX_DOKUD; }
				else { return LEX_ERROR; }

			case 19: /* 'v'..? */
				if ( znak == 'r' ){ stav = 20; break; }
				else { return LEX_ERROR; }

			case 20: /* 'vr'..? */
				if ( znak == 'a' ){ stav = 21; break; }
				else { return LEX_ERROR; }

			case 21: /* 'vra'..? */
				if ( znak == 't' ){ return LEX_VRAT; }
				else { return LEX_ERROR; }

			case 22: /* '$'..? (identificator) */
				if ( isalpha(znak) ) /* prvni znak identifikatoru */
				{
					lex_ident[0] = znak;
					identpos = 1;
					stav = 23;
					break;
				}
				else { return LEX_ERROR; }

			case 23: /* $[a-zA-Z]..? (identificator) */
				if ( isalpha(znak) || isdigit(znak) || znak == '_' )
				{
					if ( identpos < (int) sizeof(lex_ident) - 1 )
					{
						lex_ident[identpos++] = znak;
					}
					break; /* stav zustava stejny */
				}
				ungetc(znak, fp); /* konec indetifikatoru */
				lex_ident[identpos] = 0;
				return LEX_IDENT;

			case 24: /* [0-9]..? (digit) */
				if ( isdigit(znak) )
				{
					lex_int   = lex_int   * 10 + znak - '0';
					lex_float = lex_float * 10 + znak - '0';
					break; /* zustava stejny stav */
				}
				if ( znak == '.' )
				{
					stav = 25;
					numpos = 0.1;
					break;
				}
				/* konec cisla */
				if ( znak == '\r' || znak == '\n' )
				{
					++linenum;
				}
				else
				{
					ungetc(znak, fp);
				}
				return LEX_INT;

			case 25: /* [0-9]+\...? (digit with dot) */
				if ( isdigit(znak) )
				{
					lex_float += numpos * ( znak - '0' );
					numpos /= 10.0;
					stav = 26;
					break;
				}
				else{ return LEX_ERROR; }

			case 26: /* number like '134.' not allowed */
				if ( isdigit(znak) )
				{
					lex_float += numpos * ( znak - '0' );
					numpos /= 10.0;
					break;
				}
				ungetc(znak, fp);
				return LEX_FLOAT;

			case 27:
				if ( znak == ' ' || znak == '\t' )
				{
					stav = 28;
					break;
				}
				if ( znak == '\r' || znak == '\n' )
				{
					stav = 28;
					linenum++;
					break;
				}
				return LEX_ERROR;

			case 28:
				if ( znak == ' ' || znak == '\t' )
				{
					break;
				}
				if ( znak == '\r' || znak == '\n' )
				{
					linenum++;
					break;
				}
				if ( isalpha(znak) )
				{
					lex_prgname[0] = znak;
					prognamepos = 1;
					stav = 29;
					break;
				}
				return LEX_ERROR;

			case 29:
				if ( isalpha(znak) || isdigit(znak) || znak == '_' )
				{
					if ( prognamepos < (int) sizeof(lex_prgname) - 1 )
					{
						lex_prgname[prognamepos++] = znak;
					}
					break;
				}
				ungetc(znak, fp);
				lex_prgname[prognamepos] = 0;
				return LEX_PROGRAM;

			case 30: /* 'j'..? */
				if ( znak == 'i' ){ stav = 31; break; }
				return LEX_ERROR;

			case 31: /* 'ji'..? */
				if ( znak == 'n' ){ stav = 32; break; }
				return LEX_ERROR;

			case 32: /* 'jin'..? */
				if ( znak == 'a' ){ stav = 33; break; }
				return LEX_ERROR;

			case 33: /* 'jina'..? */
				if ( znak == 'k' ){ return LEX_JINAK; }
				return LEX_ERROR;

			case 34: /* 'promenn'..? */
				if ( znak == 'a' ){ return LEX_PROMENA; }
				else { return LEX_ERROR; }

			case 35: /* 'i'..? */
				if ( znak == 'n' ){ stav = 36; break; }
				else { return LEX_ERROR; }

			case 36: /* 'in'..? */
				if ( znak == 't' ){ return LEX_INT_KW; }
				else { return LEX_ERROR; }

			case 37: /* 'f'..? */
				if ( znak == 'l' ){ stav = 38; break; }
				else { return LEX_ERROR; }

			case 38: /* 'fl'..? */
				if ( znak == 'o' ){ stav = 39; break; }
				else { return LEX_ERROR; }

			case 39: /* 'flo'..? */
				if ( znak == 'a' ){ stav = 40; break; }
				else { return LEX_ERROR; }

			case 40: /* 'floa'..? */
				if ( znak == 't' ){ return LEX_FLOAT_KW; }
				else { return LEX_ERROR; }


		}
	}
}

/* EOF */
