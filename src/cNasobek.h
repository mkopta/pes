/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cNasobek.h
 * hlavickovy soubor pro tridu cNasobek
 */

#ifndef __cNasobek_h_34522334532324389778926
#define __cNasobek_h_34522334532324389778926

class cFaktor;

#include "cUzel.h"
#include "cFaktor.h"

class cNasobek : public cUzel
{
	private:
		cFaktor   *levy;
		cFaktor   *pravy;

	public:
		         cNasobek  ( cFaktor levy, cFaktor pravy );
		         cNasobek  ( const cNasobek& n );
		virtual ~cNasobek  ( );
		void     eval      ( );
		string   getTyp    ( );
};

#endif /* __cNasobek_h_34522334532324389778926 */

/* EOF */
