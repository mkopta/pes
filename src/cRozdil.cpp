/* cRozdil.cpp
 * Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * trida cRozdil, generuje kod pro vypocet rozdilu
 */

#include <iostream>
#include "cRozdil.h"

using namespace std;

cRozdil::cRozdil ( cTerm levy, cTerm pravy )
{
	cTerm    *l = new cTerm ( levy  );
	cTerm    *p = new cTerm ( pravy );
	this->levy  = l;
	this->pravy = p;
}

cRozdil::cRozdil ( const cRozdil& r )
{
	levy  = r.levy;
	pravy = r.pravy;
}

cRozdil::~cRozdil()
{ /* nothing here */ }

void cRozdil::eval ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	bool x = ( ltyp != rtyp ); // jeden z nich je urcite float
	
	(*this->pravy).eval();
	if ( x && rtyp == "int" ) cout << "i2d" << endl;
	(*this->levy).eval();
	if ( x && ltyp == "int" ) cout << "i2d" << endl;

	if ( x || ltyp == "float" ) cout << "dsub" << endl;
	else cout << "isub" << endl;
}

string cRozdil::getTyp ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	if ( ltyp == "float" || rtyp == "float" )
		return "float";
	else
		return "int";
}

/* EOF */
