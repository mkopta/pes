/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cFaktor.cpp
 * trida cFaktor, generuje kod pro cast vyrazu
 */

#include <iostream>

#include "cFaktor.h"
#include "cTS.h"

using namespace std;

cFaktor::cFaktor    ( cInt oint )
{
	cInt *i = new cInt ( oint );
	this->empty   = false;
	this->oint    = i;
	this->ofloat  = 0;
	this->oident  = 0;
	this->nasobek = 0;
	this->podil   = 0;
}

cFaktor::cFaktor    ( cFloat ofloat )
{
	cFloat *f = new cFloat ( ofloat );
	this->empty   = false;
	this->oint    = 0;
	this->ofloat  = f;
	this->oident  = 0;
	this->nasobek = 0;
	this->podil   = 0;
}

cFaktor::cFaktor    ( cIdentifikator oident )
{
	cIdentifikator *i = new cIdentifikator ( oident );
	this->empty   = false;
	this->oint    = 0;
	this->ofloat  = 0;
	this->oident  = i;
	this->nasobek = 0;
	this->podil   = 0;
}

cFaktor::cFaktor    ( cNasobek nasobek )
{
	cNasobek *n = new cNasobek ( nasobek );
	this->empty   = false;
	this->oint    = 0;
	this->ofloat  = 0;
	this->oident  = 0;
	this->nasobek = n;
	this->podil   = 0;
}

cFaktor::cFaktor    ( cPodil podil )
{
	cPodil *p = new cPodil ( podil );
	this->empty   = false;
	this->oint    = 0;
	this->ofloat  = 0;
	this->oident  = 0;
	this->nasobek = 0;
	this->podil   = p;
}

cFaktor::cFaktor    ( const cFaktor& f )
{
	empty   = f.empty;
	oint    = f.oint;
	ofloat  = f.ofloat;
	oident  = f.oident;	
	nasobek = f.nasobek;
	podil   = f.podil;
}

cFaktor::cFaktor    ( )
{
	this->empty = true;
}

cFaktor::~cFaktor   ( )
{ /* nothing here */ }

string cFaktor::getTyp ( )
{
	if ( this->oint ) 
	{
		return "int";
	}
	else if ( this->ofloat )
	{
		return "float";
	}
	else if ( this->oident )
	{
		string identname = (*this->oident).getName();
		string typ = cTS::instance()->getTyp ( identname );
		return typ;
	} // dal se to nesmi dostat v pripade VRAT, PODMINKA  atp.
	else if ( this->nasobek )
	{
		string typ = (*this->nasobek).getTyp ( );
		return typ;
	}
	else if ( this->podil )
	{
		string typ = (*this->podil).getTyp ( );
		return typ;
	}
	else
	{
		return "int"; // FIXME, urcite?
	}
}

void cFaktor::eval ( )
{
	if ( empty ) return;

	if ( this->oint ) 
	{
		(*this->oint).eval();
	}
	else if ( this->ofloat )
	{
		(*this->ofloat).eval();
	}
	else if ( this->oident )
	{
		(*this->oident).eval();
	}
	else if ( this->nasobek )
	{
		(*this->nasobek).eval();
	}
	else if ( this->podil )
	{
		(*this->podil).eval();
	}
}

/* EOF */
