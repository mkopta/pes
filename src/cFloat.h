/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cFloat.h
 * hlavickovy soubor pro tridu cFloat
 */

#ifndef __cFloat_h_0980980973809238747089238
#define __cFloat_h_0980980973809238747089238

#include "cUzel.h"

class cFloat : public cUzel
{
	private:
		float     hodnota;

	public:
		cFloat    ( float hodnota );
		cFloat    ( const cFloat& f );
		virtual ~cFloat   ( );
		void eval ( );
};

#endif /* __cFloat_h_0980980973809238747089238 */

/* EOF */
