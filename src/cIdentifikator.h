/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cIdentifikator.h
 * hlavickovy soubor pro tridu cIdentifikator
 */

#ifndef __cIdentifikator_h_83920708998283090
#define __cIdentifikator_h_83920708998283090

#include "cUzel.h"

class cIdentifikator : public cUzel
{
	private:
		std::string     jmeno;
		int             id;

	public:
		cIdentifikator  ( std::string jmeno );
		cIdentifikator  ( const cIdentifikator& i );
		virtual ~cIdentifikator ( );
		void   eval     ( );
		void   eval_st  ( );
		string getName  ( );
};

#endif /* __cIdentifikator_h_83920708998283090 */

/* EOF */
