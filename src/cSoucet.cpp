/* Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cSoucet.cpp
 * trida cSoucet, generuje kod pro soucet
 */

#include <iostream>
#include "cSoucet.h"
#include "cTerm.h"

using namespace std;

cSoucet::cSoucet ( cTerm levy, cTerm pravy )
{ 
	cTerm    *l = new cTerm ( levy  );
	cTerm    *p = new cTerm ( pravy );
	this->levy  = l;
	this->pravy = p;
}

cSoucet::cSoucet ( const cSoucet& s )
{
	levy  = s.levy;
	pravy = s.pravy;
}

cSoucet::~cSoucet()
{ /* nothing here */ }

void cSoucet::eval ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	bool x = ( ltyp != rtyp ); // jeden z nich je urcite float
	
	(*this->pravy).eval();
	if ( x && rtyp == "int" ) cout << "i2d" << endl;
	(*this->levy).eval();
	if ( x && ltyp == "int" ) cout << "i2d" << endl;

	if ( x || ltyp == "float" ) cout << "dadd" << endl;
	else cout << "iadd" << endl;
}

string cSoucet::getTyp ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	if ( ltyp == "float" || rtyp == "float" )
		return "float";
	else
		return "int";
}

/* EOF */
