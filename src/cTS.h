/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cTS.h
 * hlavickovy soubor pro tridu TS
 */

#ifndef __cTS_h_5253478899996123523413
#define __cTS_h_5253478899996123523413

using namespace std;

#include <map>
#include <list>

class cTS // SINGLETON!
{
	private:
		map<string, int> A;
		map<int, string> B;
		int lastID;
		int lastLAB;
		cTS  ( );
		~cTS ( );
		static cTS* tsinstance;

	public:
		static cTS* instance ( )
		{
			if ( tsinstance == 0 )
			{
				tsinstance = new cTS;
			}
			return tsinstance;
		}
		int    getID        ( string identname );
		string getTyp       ( string identname );
		bool   jeDeklarovan ( string identname );
		void   add          ( string identname, string typ );
		int    getNextLab   ( ) ;
		string getIdentname ( int ID );
		int    getPocetProm ( ) ;
		std::list<int> getIDpromennych ( ) ;
};

#endif /* __cTS_h_5253478899996123523413 */

/* EOF */
