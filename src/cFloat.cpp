/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cFloat.cpp
 * trida cFloat, generuje kod pro praci s realnymi cisly
 */

#include <iostream>
#include <sstream>
#include "cFloat.h"

using namespace std;

cFloat::cFloat ( float hodnota )
{
	this->hodnota = hodnota;
}

cFloat::cFloat ( const cFloat& f)
{
	hodnota = f.hodnota;
}

cFloat::~cFloat()
{ /* nothing here */ }

void cFloat::eval ()
{
	//printf("ldc2_w %f\n", this->hodnota);
	cout << "ldc2_w ";
	cout << showpoint << this->hodnota;
	cout << endl;
}

/* EOF */
