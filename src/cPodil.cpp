/* Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPodil.cpp
 * trida cPodil, generuje kod pro vypocet podilu
 */

class cFaktor;

#include <iostream>
#include "cPodil.h"
#include "cFaktor.h"

using namespace std;


cPodil::cPodil ( cFaktor levy, cFaktor pravy )
{
	cFaktor *l  = new cFaktor ( levy  );
	cFaktor *p  = new cFaktor ( pravy );
	this->levy  = l;
	this->pravy = p;
}

cPodil::cPodil ( const cPodil& p )
{
	levy  = p.levy;
	pravy = p.pravy;
}

cPodil::~cPodil ( )
{ /* nothing here */ }

void cPodil::eval ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	bool x = ( ltyp != rtyp ); // jeden z nich je urcite float
	
	(*this->pravy).eval();
	if ( x && rtyp == "int" ) cout << "i2d" << endl;
	(*this->levy).eval();
	if ( x && ltyp == "int" ) cout << "i2d" << endl;

	if ( x || ltyp == "float" ) cout << "ddiv" << endl;
	else cout << "idiv" << endl;
}

string cPodil::getTyp ( )
{
	string ltyp = (*this->levy).getTyp  ( );
	string rtyp = (*this->pravy).getTyp ( );
	if ( ltyp == "float" || rtyp == "float" )
		return "float";
	else
		return "int";
}

/* EOF */
