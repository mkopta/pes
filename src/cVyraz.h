/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cVyraz.h
 * hlavickovy soubor pro tridu cVyraz
 */

#ifndef __cVyraz_h_9089092999273484673737828
#define __cVyraz_h_9089092999273484673737828

#include "cUzel.h"
#include "cTerm.h"

class cVyraz : public cUzel
{
	private:
		cTerm     *term;
		bool      empty;

	public:
		cVyraz    ( );
		cVyraz    ( cTerm term );
		cVyraz    ( const cVyraz& v );
		virtual ~cVyraz   ( );
		void eval ( );
		string getTyp ( );
};

#endif /* __cVyraz_h_9089092999273484673737828 */

/* EOF */

