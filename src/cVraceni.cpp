/* cVraceni.cpp
 * Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * date
 * Dec 2008
 * trida cVraceni, generuje kod pro vraceni
 */

#include <iostream>
#include "cVraceni.h"
#include "cFaktor.h"

using namespace std;

cVraceni::cVraceni ( cFaktor covratit )
{
	cFaktor *f = new cFaktor ( covratit );
	this->covratit = f;
}

cVraceni::cVraceni ( const cVraceni& v )
{
	covratit = v.covratit;
}
		
cVraceni::~cVraceni()
{ /* nothing here */ }

void cVraceni::eval ()
{
	cout << "; Vraceni" << endl;
	cout << "getstatic java/lang/System/out Ljava/io/PrintStream;" << endl;
	(*this->covratit).eval();
	string typ = (*this->covratit).getTyp();
	if ( typ == "float" )
	{
		cout << "invokevirtual java/io/PrintStream/println(D)V" << endl;
	}
	else // if ( typ == "int" )
	{
		cout << "invokevirtual java/io/PrintStream/println(I)V" << endl;
	}
	cout << "return" << endl;
}

/* EOF */
