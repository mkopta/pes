/* (c) 2008, martin 'dum8d0g' kopta <martin@kopta.eu>
 * Dec 2008
 * cVraceni.h
 * hlavickovy soubor pro tridu cVraceni
 */

#ifndef __cVraceni_h_65754792837916783897949
#define __cVraceni_h_65754792837916783897949


#include "cUzel.h"
#include "cFaktor.h"

class cVraceni : public cUzel
{
	private:
		cFaktor  *covratit;

	public:
		cVraceni  ( cFaktor covratit );
		cVraceni  ( const cVraceni& v );
		virtual ~cVraceni ( );
		void eval ( );
};


#endif /* __cVraceni_h_65754792837916783897949 */

/* EOF */
