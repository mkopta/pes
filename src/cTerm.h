/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cTerm.h
 * hlavickovy soubor pro tridu cTerm
 */

#ifndef __cTerm_h_22124235268962737372
#define __cTerm_h_22124235268962737372

class cSoucet;
class cRozdil;

#include "cUzel.h"
#include "cSoucet.h"
#include "cRozdil.h"
#include "cZbTermu.h"

class cTerm : public cUzel
{
	private:
		cSoucet  *soucet;
		cRozdil  *rozdil;
		cZbTermu *zbtermu;
		bool     empty;

	public:
		         cTerm  ( );
		         cTerm  ( cRozdil rozdil );
		         cTerm  ( cSoucet soucet );
		         cTerm  ( cZbTermu zbtermu );
		         cTerm  ( const cTerm& t );
		virtual ~cTerm  ( );
		void     eval   ( );
		string   getTyp ( );
};

#endif /* __cTerm_h_22124235268962737372 */

/* EOF */

