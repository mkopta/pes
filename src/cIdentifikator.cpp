/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cIdentifikator.cpp
 * trida cIdentifikator, generuje kod pro praci s promennymi
 */

#include <iostream>
#include "cIdentifikator.h"
#include "cTS.h"

using namespace std;

cIdentifikator::cIdentifikator ( std::string jmeno )
{
	this->jmeno = jmeno;
}

cIdentifikator::cIdentifikator ( const cIdentifikator& i )
{
	jmeno = i.jmeno;
	id    = i.id;
}

cIdentifikator::~cIdentifikator()
{ /* nothing here */ }

void cIdentifikator::eval ()
{
	int    id  = cTS::instance()->getID  ( jmeno );
	string typ = cTS::instance()->getTyp ( jmeno );
	if ( typ == "float" ) cout << "d";
	if ( typ == "int"   ) cout << "i";
	cout << "load ";
	cout << id;
	cout << " ;" << jmeno << endl;
}

void cIdentifikator::eval_st ( )
{
	int    id  = cTS::instance()->getID  ( jmeno );
	string typ = cTS::instance()->getTyp ( jmeno );
	if ( typ == "float" ) cout << "d";
	if ( typ == "int"   ) cout << "i";
	cout << "store ";
	cout << id;
	cout << endl;
}

string cIdentifikator::getName ( )
{
	return this->jmeno;
}

/* EOF */
