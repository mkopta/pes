/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cFaktor.h
 * hlavickovy soubor pro tridu cFaktor
 */

#ifndef __cFaktor_h_627687672562311314
#define __cFaktor_h_627687672562311314

class cNasobek;
class cPodil;

#include "cUzel.h"
#include "cInt.h"
#include "cFloat.h"
#include "cIdentifikator.h"
#include "cNasobek.h"
#include "cPodil.h"
#include "cTS.h"

class cFaktor : public cUzel
{
	private:
		cInt           *oint;
		cFloat         *ofloat;
		cIdentifikator *oident;	
		cNasobek       *nasobek;
		cPodil         *podil;
		bool            empty;

	public:
		cFaktor    ( cInt           oint );
		cFaktor    ( cFloat         ofloat );
		cFaktor    ( cIdentifikator oident );
		cFaktor    ( cNasobek       nasobek );
		cFaktor    ( cPodil         podil );
		cFaktor    ( const cFaktor& f );
		cFaktor    ( );
		virtual ~cFaktor   ( );
		void eval  ( );
		std::string getTyp ( );
};

#endif /* __cFaktor_h_627687672562311314 */

/* EOF */
