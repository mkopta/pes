/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPodil.h
 * hlavickovy soubor pro tridu cPodil
 */

#ifndef __cPodil_h_5476235623708789473986576
#define __cPodil_h_5476235623708789473986576

#include "cUzel.h"
#include "cFaktor.h"

class cPodil : public cUzel
{
	private:
		cFaktor *levy;
		cFaktor *pravy;

	public:
		         cPodil ( cFaktor levy, cFaktor pravy );
		         cPodil ( const cPodil& p );
		virtual ~cPodil ( );
		void     eval   ( );
		string   getTyp ( );
};

#endif /* __cPodil_h_5476235623708789473986576 */

/* EOF */
