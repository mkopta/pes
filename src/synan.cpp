/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Nov 2008
 * synan.c
 * Syntakticky analyzator jazyka pes
 *
 * Pomoci rekurzivniho sestupu projde vsechny tokeny od lexanu
 * a pro kazdy sestup provede nejakou akci
 */

#include <stdlib.h>
#include <iostream>
#include <string>
#include <map>
#include <list>

#include "synan.h"
#include "lexan.h"
#include "cTS.h"

#include "cUzel.h"
#include "cBlok.h"
#include "cPrikaz.h"
#include "cPodminka.h"
#include "cDokud.h"
#include "cPokud.h"
#include "cPodminka.h"
#include "cVraceni.h"
#include "cPrirazeni.h"
#include "cIdentifikator.h"
#include "cInt.h"
#include "cFloat.h"
#include "cSoucet.h"
#include "cRozdil.h"
#include "cNasobek.h"
#include "cPodil.h"
#include "cProgram.h"
#include "cTerm.h"
#include "cVyraz.h"
#include "cFaktor.h"
#include "cZbTermu.h"

using namespace std;

/* Blok */
static cBlok Blok           ( void );
static cBlok BlokI          ( void );
/* Cislo */
static float Cislo          ( void );
/* Cyklus */
static cDokud Cyklus        ( void );
/* Deklarace */
static void Deklarace       ( void );
static string Typ           ( void );
/* Identifikator */
static string Identifikator ( void );
/* Podminka */
static cPodminka Podminka   ( void );
static cFaktor PodminkaI    ( void );
static string PodminkaII    ( void );
/* Prikaz */
static cPrikaz Prikaz       ( void );
/* Start programu */
static cProgram Program     ( void );
static void ProgramI        ( void );
/* Vetveni */
static cPokud Vetveni       ( void );
static cBlok VetveniI       ( void );
/* Vraceni */
static cVraceni Vraceni     ( void );
static cFaktor VraceniI     ( void );
/* Vypocet */
static cPrirazeni Vypocet   ( void );
static cVyraz ZbVypoctu     ( cTerm levy );
static cTerm Term           ( void );
static cZbTermu ZbTermu     ( cFaktor levy );
static cFaktor Faktor       ( void );

static int token = 0;

void error (void)
{
	cerr << "CHYBA ve vstupnim souboru. Zkontrolujte syntaxi." << endl;
	cerr << "Problem je na radku " << linenum << "." << endl;
	if ( unknown_char != '\0' )
	{
		cerr << "Neznamy element '" << unknown_char << "'." << endl;
	}
	exit(1);
}

void checkident ( string identname )
{
	bool exists;
	exists = cTS::instance()->jeDeklarovan ( identname );
	if ( exists == false )
	{
		cerr << "Promenna '" << identname << "' nebyla deklarovana" << endl;
		error ();
	}
}

cProgram parse (char *filename)
{
	/* Otevreni vstupniho souboru */
	if ( ! lexanInit(filename) )
	{
		cerr << "CHYBA nemuzu otevrit soubor '" << filename << "'" << endl;
		exit(1);
	}
	/* Predpripraveni prvniho tokenu */
	token = getNextToken();
	/* Start rekurzivniho sestupu */
	cProgram program = Program();
	/* Kontrola zda jsme precetli vsechno */
	if ( token != LEX_NULL )
	{
		cerr << "CHYBA ve vstupnim souboru. Kontrola probehla v ";
		cerr << "poradku, ale v souboru je jeste nejake smeti ";
		cerr << "na konci. Zkontrolujte syntaxi." << endl;
		error();
	}
	/* Zavreni souboru */
	lexanDone();
	return program;
}

cProgram Program (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_PROGRAM:
		{
			string progname = lex_prgname;
			token = getNextToken();
			ProgramI(); // nic nevraci, jen deklaruje
			cProgram *program = new cProgram ( progname, Blok() );
			return *program;
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cProgram() );
}

void ProgramI (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_PROMENA:
			Deklarace();
			ProgramI();
			return;

		/* FOLLOW */
		case LEX_LCB:
			return;

		default:
			error();
	}
}

void Deklarace (void)
{
	switch ( token ) 
	{
		/* FIRST */
		case LEX_PROMENA:
		{
			token = getNextToken();
			string typ = Typ();
			string identname = Identifikator();
			bool x = cTS::instance()->jeDeklarovan( identname );
			if ( x == true )
			{
				cerr << "Promenna '" << identname << "je jiz deklarovana!";
				cerr << " Promennou lze deklarovat jen jednou. Prosim, ";
				cerr << "zkontrolujte syntaxi." << endl;
				error();
			}
			else // symbol jeste nadeklarovan nebyl
			{
				cTS::instance()->add ( identname, typ );
			}
			return;
		}

		default:
			error();
	}
}

string Typ (void)
{
	switch ( token )
	{
		case LEX_INT_KW:
			token = getNextToken();
			return ( "int" );

		case LEX_FLOAT_KW:
			token = getNextToken();
			return ( "float" );

		default:
			cerr << "Zrejme jste zapomeli uvest datovy typ promenne." << endl;
			cerr << "Na vyber mate 'int' a 'float'." << endl;
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return "";
}

cBlok Blok (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_LCB:
			token = getNextToken();
			return BlokI();

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cBlok() );
}

cBlok BlokI (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_POKUD:
		case LEX_DOKUD:
		case LEX_VRAT:
		case LEX_IDENT:
		{
			cPrikaz prikaz = Prikaz();
			cBlok   next   = BlokI();
			cBlok   *retval = new cBlok ( prikaz, next );
			return ( *retval );
		}

		case LEX_RCB:
		{
			token = getNextToken();
			return *( new cBlok() ); // prazdny prikaz
		}

		case LEX_NULL:
		{
			return *( new cBlok() ); // prazdny prikaz
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cBlok() );
}

cPrikaz Prikaz (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_POKUD:
			return *( new cPrikaz ( Vetveni() ) );

		case LEX_DOKUD:
			return *( new cPrikaz ( Cyklus() ) );

		case LEX_VRAT:
			return *( new cPrikaz ( Vraceni() ) );

		case LEX_IDENT:
			return *( new cPrikaz ( Vypocet() ) );

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cPrikaz() );
}

float Cislo (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_INT:
			token = getNextToken();
			return ( (float)lex_int );

		case LEX_FLOAT:
			token = getNextToken();
			return ( lex_float );

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return 0.0;
}

cDokud Cyklus (void)
{
	if ( token != LEX_DOKUD ) error();
	token = getNextToken();
	cPodminka podminka = Podminka();
	cBlok     blok     = Blok();
	cDokud    retval   = *( new cDokud ( podminka, blok ) );
	return ( retval );
}

string Identifikator (void)
{
	switch ( token ) 
	{
		/* FIRST */
		case LEX_IDENT:
		{
			string identname = lex_ident;
			token = getNextToken();
			return ( identname ); 
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return "";
}

cPodminka Podminka (void)
{
	if ( token != LEX_LBB ) error();
	token = getNextToken();

	cFaktor levy     = PodminkaI () ;
	string relace  = PodminkaII () ;
	cFaktor pravy    = PodminkaI () ;

	if ( token != LEX_RBB ) error();
	token = getNextToken();

	return *( new cPodminka ( levy, relace, pravy ) );
}

cFaktor PodminkaI (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_IDENT:
		{
			string         name   = Identifikator();
			checkident ( name );
			cIdentifikator ident  = *( new cIdentifikator ( name ) );
			cFaktor        retval = *( new cFaktor ( ident ) );
			return retval;
		}
			
		case LEX_INT:
		{
			int     cislo  = (int) Cislo();
			cInt    oint   = *( new cInt ( cislo ) );
			cFaktor retval = *( new cFaktor ( oint ) );
			return retval;
		}

		case LEX_FLOAT:
		{
			float   cislo  = Cislo();
			cFloat  ofloat = *( new cFloat ( cislo ) );
			cFaktor retval = *( new cFaktor ( ofloat ) );
			return retval;
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cFaktor() );
}

string PodminkaII (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_IEQ:
			token = getNextToken();
			return ( "==" );

		case LEX_NEQ:
			token = getNextToken();
			return ( "!=" );

		case LEX_LAB:
			token = getNextToken();
			return ( "<" );

		case LEX_RAB:
			token = getNextToken();
			return ( ">" );

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return "";
}

cPokud Vetveni (void)
{
	if ( token != LEX_POKUD ) error();
	token = getNextToken();

	cPodminka podminka = Podminka();
	cBlok     blok     = Blok();
	cBlok     vetveniI = VetveniI();
	cPokud    pokud    = *( new cPokud ( podminka, blok, vetveniI ) );
	return pokud;
}

cBlok VetveniI (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_JINAK:
			token = getNextToken();
			return ( Blok() );

		/* FOLLOW */
		case LEX_LCB:
		case LEX_POKUD:
		case LEX_DOKUD:
		case LEX_VRAT:
		case LEX_IDENT:
			// TODO otestovat jestli je to dobre
			return *( new cBlok() ); // vrati prazdny uzel, ktery nevygeneruje nic

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cBlok() );
}

cVraceni Vraceni (void)
{
	if ( token != LEX_VRAT ) error();
	token = getNextToken();

	cFaktor  vraceniI  = VraceniI();
	cVraceni retval    = *( new cVraceni ( vraceniI ) );
	return retval;
}

cFaktor VraceniI (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_IDENT:
		{
			string         name   = Identifikator();
			checkident ( name );
			cIdentifikator ident  = *( new cIdentifikator ( name ) );
			cFaktor        retval = *( new cFaktor ( ident ) );
			return retval;
		}
			
		case LEX_INT:
		{
			int     cislo  = (int) Cislo();
			cInt    oint   = *( new cInt ( cislo ) );
			cFaktor retval = *( new cFaktor ( oint ) );
			return retval;
		}

		case LEX_FLOAT:
		{
			float   cislo  = Cislo();
			cFloat  ofloat = *( new cFloat ( cislo ) );
			cFaktor retval = *( new cFaktor ( ofloat ) );
			return retval;
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cFaktor() );
}

cPrirazeni Vypocet (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_IDENT:
		{
			string identname = Identifikator();

			if ( token != LEX_EQ ) error();
			token = getNextToken();

			checkident ( identname );
			cIdentifikator ident = *( new cIdentifikator ( identname ) );

			cTerm term           = Term ();
			cVyraz zbvypoctu     = ZbVypoctu( term );
			cPrirazeni prirazeni = *( new cPrirazeni ( ident, zbvypoctu ) );
			return prirazeni;
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cPrirazeni() );
}


cVyraz ZbVypoctu ( cTerm levy )
{
	switch ( token )
	{
		/* FIRST */
		case LEX_ADD:
		{
			token = getNextToken();

			cTerm   term      = Term();
			cSoucet soucet    = *( new cSoucet ( term, levy ) );
			cTerm   chuchel   = *( new cTerm ( soucet ) );
			cVyraz  zbvypoctu = ZbVypoctu ( chuchel );
			return zbvypoctu;
		}

		case LEX_SUB:
		{
			token = getNextToken();

			cTerm   term      = Term();
			cRozdil rozdil    = *( new cRozdil ( term, levy ) );
			cTerm   chuchel   = *( new cTerm ( rozdil ) );
			cVyraz  zbvypoctu = ZbVypoctu ( chuchel );
			return  zbvypoctu;
		}

		/* FOLLOW */
		case LEX_RCB:
		case LEX_POKUD:
		case LEX_DOKUD:
		case LEX_VRAT:
		case LEX_IDENT:
			return *( new cVyraz ( levy ) );

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cVyraz() );
}

cTerm Term (void)
{
	switch ( token )
	{
		/* FIRST */
		case LEX_IDENT:
		case LEX_INT:
		case LEX_FLOAT:
		{
			// (28)
			cFaktor  faktor  = Faktor();
			cZbTermu zbtermu = ZbTermu ( faktor );
			cTerm    retval  = *( new cTerm ( zbtermu ) );
			return retval;
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cTerm() );
}

cZbTermu ZbTermu (cFaktor levy)
{
	switch (token)
	{
		/* FIRST */
		case LEX_MUL:
		{
			token = getNextToken();
			// (29)
			cFaktor  faktor  = Faktor();
			cNasobek nasobek = *( new cNasobek ( faktor, levy ) );
			cFaktor  chuchel = *( new cFaktor ( nasobek ) );
			cZbTermu retval  = ZbTermu ( chuchel );
			return retval;
		}

		case LEX_DIV:
		{
			token = getNextToken();
			// (30)
			cFaktor  faktor  = Faktor();
			cPodil   podil   = *( new cPodil ( faktor, levy ) );
			cFaktor  chuchel = *( new cFaktor ( podil ) );
			cZbTermu retval  = ZbTermu ( chuchel );
			return retval;
		}

		/* FOLLOW */
		case LEX_ADD:
		case LEX_SUB:
		case LEX_RCB:
		case LEX_POKUD:
		case LEX_DOKUD:
		case LEX_VRAT:
		case LEX_IDENT:
		{
			cZbTermu retval = *( new cZbTermu( levy ) );
			return retval; // (31)
		}

		default:
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cZbTermu() );
}

cFaktor Faktor (void)
{
	switch ( token ) 
	{
		/* FIRST */
		case LEX_IDENT:
		{
			string         name   = Identifikator();
			checkident ( name );
			cIdentifikator ident  = *( new cIdentifikator ( name ) );
			cFaktor        retval = *( new cFaktor ( ident ) );
			return retval;
		}
			
		case LEX_INT:
		{
			int     cislo  = (int) Cislo();
			cInt    oint   = *( new cInt ( cislo ) );
			cFaktor retval = *( new cFaktor ( oint ) );
			return retval;
		}

		case LEX_FLOAT:
		{
			float   cislo  = Cislo();
			cFloat  ofloat = *( new cFloat ( cislo ) );
			cFaktor retval = *( new cFaktor ( ofloat ) );
			return retval;
		}

		default: 
			error();
	}
	// TOTO SE NIKDY NESMI VYKONAT (jenom bezpecnostni konstrukce)
	return *( new cFaktor() );
}

/* EOF */
