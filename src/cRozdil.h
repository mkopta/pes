/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cRozdil.h
 * hlavickovy soubor pro tridu cRozdil
 */

#ifndef __cRozdil_h_537890765273828472899378
#define __cRozdil_h_537890765273828472899378

class cTerm;

#include "cUzel.h"
#include "cTerm.h"

class cRozdil : public cUzel
{
	private:
		cTerm     *levy;
		cTerm     *pravy;
	public:
		         cRozdil ( cTerm levy, cTerm pravy );
		         cRozdil ( const cRozdil& r );
		virtual ~cRozdil ( );
		void     eval    ( );
		string   getTyp  ( );
};

#endif /* __cRozdil_h_537890765273828472899378 */

/* EOF */
