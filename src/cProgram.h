/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cProgram.h
 * hlavickovy soubor pro tridu cProgram
 */

#ifndef __cProgram_h_55257699912122521766297
#define __cProgram_h_55257699912122521766297

#include "cUzel.h"
#include "cBlok.h"

class cProgram : public cUzel
{
	private:
		std::string prgname;
		cBlok      *blok;
		bool        emptyblok;
		void        initPromennych ( );

	public:
		cProgram  ( std::string prgname, cBlok blok );
		cProgram  ( const cProgram& p );
		cProgram  ( );
		virtual ~cProgram ( );
		void eval ( );
};

#endif /* __cProgram_h_55257699912122521766297 */

/* EOF */
