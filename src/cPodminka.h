/* (c) Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cPodminka.h
 * hlavickovy soubor pro tridu cPodminka
 */

#ifndef __cPodminka_h_7862785762368978293726
#define __cPodminka_h_7862785762368978293726

#include "cUzel.h"
#include "cFaktor.h"

class cPodminka : public cUzel
{
	private:
		cFaktor      *levy;
		cFaktor      *pravy;
		std::string   relace;
		std::string   label;

	public:
		cPodminka     ( cFaktor levy, std::string relace, cFaktor pravy );
		cPodminka     ( const cPodminka& p );
		virtual ~cPodminka   ( );
		void eval     ( );
		void setLabel ( std::string givenlabel );
};

#endif /* __cPodminka_h_7862785762368978293726 */

/* EOF */
