/* (c) 2008, Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cUzel.h
 * hlavickovy soubor pro tridu cUzel
 */

#ifndef __cUzel_h_7328967423087293789121980432
#define __cUzel_h_7328967423087293789121980432

using namespace std;

class cUzel
{
	private:
		/* nothing here */

	public:
		cUzel             ( );
		virtual ~cUzel            ( );
		virtual void eval ( )
		{
			cout << "; NOT IMPLEMENTED " << endl;
		}
};

#endif /* __cUzel_h_7328967423087293789121980432 */

/* EOF */
