/* Martin 'dum8d0g' Kopta <martin@kopta.eu>
 * Dec 2008
 * cZbTermu.cpp
 * trida cZbTermu, generuje kod pro cast vypoctu
 */

#include <iostream>
#include "cZbTermu.h"

using namespace std;


cZbTermu::cZbTermu ( cFaktor faktor )
{
	cFaktor   *f = new cFaktor ( faktor );
	this->empty  = false;
	this->faktor = f;
}

cZbTermu::cZbTermu ( )
{
	this->empty = true;
}

cZbTermu::cZbTermu ( const cZbTermu& z )
{
	empty  = z.empty;
	faktor = z.faktor;
}

cZbTermu::~cZbTermu ( )
{ /* nothing here */ }

void cZbTermu::eval ( )
{
	if ( empty ) return;
	(*this->faktor).eval();
}

string cZbTermu::getTyp ( )
{
	if ( empty ) return "int"; // FIXME, nikdy se nestane??
	string typ = (*this->faktor).getTyp ( );
	return typ;
}

/* EOF */
