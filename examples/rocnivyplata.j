; PROGRAM: rocnivyplata
.class public rocnivyplata
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  30
.limit locals 3
; Inicializace promennych
ldc    -273
istore 2 ;mesic
ldc    -273
istore 1 ;mesicniplat
ldc    -273
istore 0 ;rocnivyplata
; Prelozene prikazy
; Prirazeni
ldc 0
istore 0 ;rocnivyplata
; Prirazeni
ldc 10
ldc 10
imul
ldc 10
imul
ldc 10
imul
istore 1 ;mesicniplat
; Prirazeni
ldc 1
istore 2 ;mesic
; Cyklus
cyklus_0: 
iload 2 ;mesic
ldc 13
if_icmpge cyklus_konec_0
; Prirazeni
iload 0 ;rocnivyplata
iload 1 ;mesicniplat
iadd
istore 0 ;rocnivyplata
; Prirazeni
iload 2 ;mesic
ldc 1
iadd
istore 2 ;mesic
goto cyklus_0
cyklus_konec_0: 
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 0 ;rocnivyplata
invokevirtual java/io/PrintStream/println(I)V
return
; Konec sveta
return
.end method
