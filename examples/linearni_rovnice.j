; PROGRAM: linearni_rovnice
.class public linearni_rovnice
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  60
.limit locals 6
; Inicializace promennych
ldc2_w -273.15
dstore 0 ;a
ldc2_w -273.15
dstore 2 ;b
ldc2_w -273.15
dstore 4 ;x
; Prelozene prikazy
; Prirazeni
ldc2_w 4.000000
dstore 0 ;a
; Prirazeni
ldc2_w 2.000000
dstore 2 ;b
; Prirazeni
ldc 0
i2d
dload 2 ;b
dload 0 ;a
ddiv
dsub
dstore 4 ;x
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
dload 4 ;x
invokevirtual java/io/PrintStream/println(D)V
return
; Konec sveta
return
.end method
