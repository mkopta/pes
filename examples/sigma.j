; PROGRAM: sigma
.class public sigma
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  40
.limit locals 4
; Inicializace promennych
ldc    -273
istore 0 ;cislo
ldc    -273
istore 2 ;iter
ldc    -273
istore 1 ;pocet
ldc    -273
istore 3 ;tmp
; Prelozene prikazy
; Prirazeni
ldc 12
istore 0 ;cislo
; Prirazeni
ldc 0
istore 1 ;pocet
; Prirazeni
iload 0 ;cislo
istore 2 ;iter
; Cyklus
cyklus_0: 
iload 2 ;iter
ldc 0
if_icmple cyklus_konec_0
; Prirazeni
iload 0 ;cislo
iload 2 ;iter
idiv
istore 3 ;tmp
; Prirazeni
iload 0 ;cislo
iload 3 ;tmp
iload 2 ;iter
imul
isub
istore 3 ;tmp
; Vetveni
vetveni_1: 
iload 3 ;tmp
ldc 0
if_icmpne vetveni_stred_1
; Prirazeni
iload 1 ;pocet
ldc 1
iadd
istore 1 ;pocet
vetveni_stred_1: 
vetveni_konec_1: 
; Prirazeni
iload 2 ;iter
ldc 1
isub
istore 2 ;iter
goto cyklus_0
cyklus_konec_0: 
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 1 ;pocet
invokevirtual java/io/PrintStream/println(I)V
return
; Konec sveta
return
.end method
