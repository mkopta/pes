; PROGRAM: vypocet_obvodu_kruhu
.class public vypocet_obvodu_kruhu
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  50
.limit locals 5
; Inicializace promennych
ldc2_w -273.15
dstore 3 ;obvodKruhu
ldc2_w -273.15
dstore 0 ;pi
ldc    -273
istore 2 ;polomer_kruhu
; Prelozene prikazy
; Prirazeni
ldc2_w 3.140000
dstore 0 ;pi
; Prirazeni
ldc 10
istore 2 ;polomer_kruhu
; Prirazeni
ldc 2
i2d
dload 0 ;pi
dmul
iload 2 ;polomer_kruhu
i2d
dmul
dstore 3 ;obvodKruhu
; Vetveni
vetveni_0: 
dload 3 ;obvodKruhu
ldc 0
i2d
dcmpg
ifge vetveni_stred_0
; Prirazeni
ldc 0
i2d
dstore 3 ;obvodKruhu
vetveni_stred_0: 
vetveni_konec_0: 
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
dload 3 ;obvodKruhu
invokevirtual java/io/PrintStream/println(D)V
return
; Konec sveta
return
.end method
