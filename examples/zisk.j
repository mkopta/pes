; PROGRAM: zisk
.class public zisk
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  30
.limit locals 3
; Inicializace promennych
ldc    -273
istore 0 ;prijmy
ldc    -273
istore 1 ;vydaje
ldc    -273
istore 2 ;zisk
; Prelozene prikazy
; Prirazeni
ldc 20000
istore 0 ;prijmy
; Prirazeni
ldc 10000
istore 1 ;vydaje
; Prirazeni
iload 0 ;prijmy
iload 1 ;vydaje
isub
istore 2 ;zisk
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 2 ;zisk
invokevirtual java/io/PrintStream/println(I)V
return
; Konec sveta
return
.end method
