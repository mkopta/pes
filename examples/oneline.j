; PROGRAM: oneline
.class public oneline
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  10
.limit locals 1
; Inicializace promennych
ldc    -273
istore 0 ;x
; Prelozene prikazy
; Prirazeni
ldc 0
ldc 1
ldc 3
imul
isub
istore 0 ;x
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 0 ;x
invokevirtual java/io/PrintStream/println(I)V
return
; Konec sveta
return
.end method
