; PROGRAM: euklid
.class public euklid
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  30
.limit locals 3
; Inicializace promennych
ldc    -273
istore 2 ;r
ldc    -273
istore 0 ;u
ldc    -273
istore 1 ;v
; Prelozene prikazy
; Prirazeni
ldc 40902
istore 0 ;u
; Prirazeni
ldc 24140
istore 1 ;v
; Cyklus
cyklus_0: 
iload 1 ;v
ldc 0
if_icmpeq cyklus_konec_0
; Prirazeni
iload 0 ;u
iload 1 ;v
idiv
istore 2 ;r
; Prirazeni
iload 0 ;u
iload 2 ;r
iload 1 ;v
imul
isub
istore 2 ;r
; Prirazeni
iload 1 ;v
istore 0 ;u
; Prirazeni
iload 2 ;r
istore 1 ;v
goto cyklus_0
cyklus_konec_0: 
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 0 ;u
invokevirtual java/io/PrintStream/println(I)V
return
; Konec sveta
return
.end method
