; PROGRAM: faktorial
.class public faktorial
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  20
.limit locals 2
; Inicializace promennych
ldc    -273
istore 0 ;cislo
ldc    -273
istore 1 ;vysledek
; Prelozene prikazy
; Prirazeni
ldc 8
istore 0 ;cislo
; Prirazeni
ldc 1
istore 1 ;vysledek
; Cyklus
cyklus_0: 
iload 0 ;cislo
ldc 0
if_icmple cyklus_konec_0
; Prirazeni
iload 1 ;vysledek
iload 0 ;cislo
imul
istore 1 ;vysledek
; Prirazeni
iload 0 ;cislo
ldc 1
isub
istore 0 ;cislo
goto cyklus_0
cyklus_konec_0: 
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 1 ;vysledek
invokevirtual java/io/PrintStream/println(I)V
return
; Konec sveta
return
.end method
