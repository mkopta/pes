; PROGRAM: operace
.class public operace
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  100
.limit locals 10
; Inicializace promennych
ldc2_w -273.15
dstore 6 ;a
ldc2_w -273.15
dstore 8 ;b
ldc2_w -273.15
dstore 4 ;c
ldc2_w -273.15
dstore 0 ;x
ldc2_w -273.15
dstore 2 ;y
; Prelozene prikazy
; Prirazeni
ldc 0
i2d
dstore 6 ;a
; Prirazeni
ldc 1
i2d
dstore 8 ;b
; Prirazeni
ldc 2
i2d
dstore 4 ;c
; Prirazeni
ldc 0
i2d
dstore 0 ;x
; Prirazeni
ldc 0
i2d
dstore 2 ;y
; Prirazeni
dload 6 ;a
ldc 10
ldc 2
imul
i2d
dadd
dstore 0 ;x
; Prirazeni
ldc 12
ldc 200
imul
i2d
ldc 30
i2d
dload 4 ;c
dmul
dsub
dload 8 ;b
dload 6 ;a
dmul
dsub
dstore 2 ;y
; Prirazeni
dload 6 ;a
dload 8 ;b
dadd
dload 4 ;c
dadd
dload 0 ;x
dadd
dload 2 ;y
dadd
dstore 6 ;a
; Prirazeni
dload 6 ;a
ldc 2
i2d
ddiv
dstore 6 ;a
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
dload 6 ;a
invokevirtual java/io/PrintStream/println(D)V
return
; Konec sveta
return
.end method
