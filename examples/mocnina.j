; PROGRAM: mocnina
.class public mocnina
.super java/lang/Object

.method public static main([Ljava/lang/String;)V
.limit stack  80
.limit locals 8
; Inicializace promennych
ldc    -273
istore 0 ;cislo
ldc    -273
istore 1 ;exponent
ldc2_w -273.15
dstore 6 ;jedna
ldc    -273
istore 2 ;vysledek
ldc2_w -273.15
dstore 4 ;vysledekF
ldc    -273
istore 3 ;zaporno
; Prelozene prikazy
; Prirazeni
ldc 2
istore 0 ;cislo
; Prirazeni
ldc 0
ldc 7
isub
istore 1 ;exponent
; Prirazeni
iload 0 ;cislo
istore 2 ;vysledek
; Prirazeni
ldc 0
istore 3 ;zaporno
; Prirazeni
ldc2_w 1.000000
dstore 6 ;jedna
; Vetveni
vetveni_0: 
iload 1 ;exponent
ldc 0
if_icmpne vetveni_stred_0
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc 1
invokevirtual java/io/PrintStream/println(I)V
return
vetveni_stred_0: 
vetveni_konec_0: 
; Vetveni
vetveni_1: 
iload 1 ;exponent
ldc 0
if_icmpge vetveni_stred_1
; Prirazeni
ldc 1
istore 3 ;zaporno
; Prirazeni
ldc 0
iload 1 ;exponent
isub
istore 1 ;exponent
vetveni_stred_1: 
vetveni_konec_1: 
; Cyklus
cyklus_2: 
iload 1 ;exponent
ldc 1
if_icmple cyklus_konec_2
; Prirazeni
iload 2 ;vysledek
iload 0 ;cislo
imul
istore 2 ;vysledek
; Prirazeni
iload 1 ;exponent
ldc 1
isub
istore 1 ;exponent
goto cyklus_2
cyklus_konec_2: 
; Vetveni
vetveni_3: 
iload 3 ;zaporno
ldc 1
if_icmpne vetveni_stred_3
; Prirazeni
dload 6 ;jedna
iload 2 ;vysledek
i2d
ddiv
dstore 4 ;vysledekF
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
dload 4 ;vysledekF
invokevirtual java/io/PrintStream/println(D)V
return
vetveni_stred_3: 
; Vraceni
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 2 ;vysledek
invokevirtual java/io/PrintStream/println(I)V
return
vetveni_konec_3: 
; Konec sveta
return
.end method
